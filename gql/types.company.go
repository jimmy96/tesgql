package gql

type Response struct {
	Meta struct {
		Count   *int    `json:"count"`
		Status  *int    `json:"status"`
		Message *string `json:"message"`
	} `json:"meta"`
	Error *string `json:"error"`
	Data  []struct {
		About       *string      `json:"about"`
		Address     *string      `json:"address"`
		AngelistURL *interface{} `json:"angelist_url"`
		AreaCode    struct {
			ID   *int    `json:"id"`
			Name *string `json:"name"`
		} `json:"area_code"`
		Benefits []struct {
			ID   *int    `json:"id"`
			Name *string `json:"name"`
		} `json:"benefits"`
		Businesses []struct {
			ID   *int    `json:"id"`
			Name *string `json:"name"`
		} `json:"businesses"`
		City                  *int         `json:"city"`
		CompanyEngineerSize   *interface{} `json:"company_engineer_size"`
		CompanyEngineerSizeID *interface{} `json:"company_engineer_size_id"`
		CompanyFund           *interface{} `json:"company_fund"`
		CompanyFundID         *interface{} `json:"company_fund_id"`
		CompanyImages         []struct {
			ID    *int `json:"id"`
			Image struct {
				Medium    *string `json:"medium"`
				Original  *string `json:"original"`
				Thumbnail *string `json:"thumbnail"`
			} `json:"image"`
			Title *string `json:"title"`
		} `json:"company_images"`
		CompanyPresses []struct {
			CompanyID   *int         `json:"company_id"`
			Description *interface{} `json:"description"`
			ID          *int         `json:"id"`
			Title       *string      `json:"title"`
			URL         *string      `json:"url"`
		} `json:"company_presses"`
		CompanySegment struct {
			ID   *int    `json:"id"`
			Name *string `json:"name"`
		} `json:"company_segment"`
		CompanySegmentID *int `json:"company_segment_id"`
		CompanySize      struct {
			ID   *int    `json:"id"`
			Name *string `json:"name"`
		} `json:"company_size"`
		CompanySizeID *int `json:"company_size_id"`
		Cultures      []struct {
			ID   *int    `json:"id"`
			Name *string `json:"name"`
		} `json:"cultures"`
		Description *string `json:"description"`
		FbURL       *string `json:"fb_url"`
		ID          *int    `json:"id"`
		Image       struct {
			Medium    *string `json:"medium"`
			Original  *string `json:"original"`
			Thumbnail *string `json:"thumbnail"`
		} `json:"image"`
		ImageContentType  *string      `json:"image_content_type"`
		ImageFileName     *string      `json:"image_file_name"`
		InstagramURL      *string      `json:"instagram_url"`
		Investor          *interface{} `json:"investor"`
		LinkedinURL       *string      `json:"linkedin_url"`
		Name              *string      `json:"name"`
		Perks             *string      `json:"perks"`
		Phone             *string      `json:"phone"`
		Pt                *string      `json:"pt"`
		Slug              *string      `json:"slug"`
		Story             *string      `json:"story"`
		TechStack         *string      `json:"tech_stack"`
		TotalFund         *interface{} `json:"total_fund"`
		TwitterURL        *string      `json:"twitter_url"`
		URL               *string      `json:"url"`
		Vision            *interface{} `json:"vision"`
		YearFound         *interface{} `json:"year_found"`
		YoutubeChannelURL *string      `json:"youtube_channel_url"`
		YoutubeURL        *string      `json:"youtube_url"`
	} `json:"data"`
}
