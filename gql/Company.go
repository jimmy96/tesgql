package gql

import (
	"encoding/json"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/parnurzeal/gorequest"
)

type CompanyDetail struct {
	//CompanySize           *CompanySize       `json:"company_size"`

	//CompanyFund           *CompanyFund       `json:"company_fund"`
	//CompanyEngineerSize   *CompanyEngineerSize	`json:"company_engineer_size"`
}


var CompanySegmentType = graphql.NewObject(graphql.ObjectConfig{
	Name:   "CompanySegment",
	Fields: graphql.Fields{
		"id": &graphql.Field{Type:graphql.Int},
		"name": &graphql.Field{Type:graphql.String},
		"created_at": &graphql.Field{Type:graphql.DateTime},
		"updated_at": &graphql.Field{Type:graphql.DateTime},
	},
})

var CompanySizeType = graphql.NewObject(graphql.ObjectConfig{
	Name:   "CompanySize",
	Fields: graphql.Fields{
		"id": &graphql.Field{Type:graphql.Int},
		"name": &graphql.Field{Type:graphql.String},
		"created_at": &graphql.Field{Type:graphql.DateTime},
		"updated_at": &graphql.Field{Type:graphql.DateTime},
		"score": &graphql.Field{Type:graphql.Int},
	},
})

var CompanyFundType = graphql.NewObject(graphql.ObjectConfig{
	Name:   "CompanyFund",
	Fields: graphql.Fields{
		"id": &graphql.Field{Type:graphql.Int},
		"name": &graphql.Field{Type:graphql.String},
		"created_at": &graphql.Field{Type:graphql.DateTime},
		"updated_at": &graphql.Field{Type:graphql.DateTime},
	},
})

var CompanyEngineerSizeType = graphql.NewObject(graphql.ObjectConfig{
	Name:   "CompanyEngineerSize",
	Fields: graphql.Fields{
		"id": &graphql.Field{Type:graphql.Int},
		"name": &graphql.Field{Type:graphql.String},
		"created_at": &graphql.Field{Type:graphql.DateTime},
		"updated_at": &graphql.Field{Type:graphql.DateTime},
	},
})

var CompanyFields = graphql.Fields{
	"id": &graphql.Field{Type:graphql.Int},
	"name": &graphql.Field{Type:graphql.String},
	"pt": &graphql.Field{Type:graphql.String},
	"address":      &graphql.Field{Type: graphql.String},
	"company_segment": &graphql.Field{Type: CompanySegmentType},
	"company_size": &graphql.Field{Type:CompanySizeType},
	"url": &graphql.Field{Type: graphql.String},
	"created_at": &graphql.Field{Type: graphql.DateTime},
	"updated_at": &graphql.Field{Type: graphql.DateTime},
	"year_found": &graphql.Field{Type:graphql.Int},
	"vision": &graphql.Field{Type: graphql.String},
	"angelist_url": &graphql.Field{Type: graphql.String},
	"tech_stack": &graphql.Field{Type: graphql.String},
	"perks": &graphql.Field{Type: graphql.String},
	"story": &graphql.Field{Type: graphql.String},
	"company_fund": &graphql.Field{Type:CompanyFundType},
	"investor": &graphql.Field{Type: graphql.String},
	"total_fund":  &graphql.Field{Type: graphql.Int},
	"youtube_url":  &graphql.Field{Type: graphql.String},
	"company_engineer_size": &graphql.Field{Type:CompanyEngineerSizeType},
	"phone":        &graphql.Field{Type: graphql.String},
	"about":        &graphql.Field{Type: graphql.String},
	"verified_at": &graphql.Field{Type: graphql.DateTime},
	"city":  &graphql.Field{Type: graphql.Int},
	"twitter_url":  &graphql.Field{Type: graphql.String},
	"fb_url":  &graphql.Field{Type: graphql.String},
	"linkedin_url": &graphql.Field{Type: graphql.String},
	"instagram_url": &graphql.Field{Type: graphql.String},
	"youtube_channel_url": &graphql.Field{Type: graphql.String},
	"description": &graphql.Field{Type: graphql.String},
	"slug": &graphql.Field{Type: graphql.String},
}

var CompanyType = graphql.NewObject(graphql.ObjectConfig{
	Name:   "Company",
	Fields: CompanyFields,
})

var QueryType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Query",
	Fields: graphql.Fields{
		"company": &graphql.Field{
			Name: "Company",
			Type: CompanyType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type:        graphql.NewNonNull(graphql.Int),
					Description: "ID of the Company",
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				id, ok := p.Args["id"].(int)
				if ok {
					res, body, errs := gorequest.New().Get(fmt.Sprintf("http://devel.ekrut.com:4006/api/v1/companies?id=%v", id)).End()
					if len(errs) >0 { return nil, errs[0] }
					if res.StatusCode == 200 {
						var result Response
						err := json.Unmarshal([]byte(body), &result)
						if err != nil {
							return nil, err
						}
						return result.Data[0], err
					}
				}
				return nil, nil
			},
			Description: "Get Company Profile by ID",
		},
	},
})
