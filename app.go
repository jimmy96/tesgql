package main

import (
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"gitlab.com/ekrut_dev/testgql/gql"
	"log"
	"net/http"
)

func main() {
	/* test
	// Schema
	fields := graphql.Fields{
		"visit_count": &graphql.Field{
			Type: graphql.String,
			Resolve: func(p graphql.ResolveParams) (i interface{}, e error) {
				return 100, nil
			},
			Description: "Hello World",
		},
		"active_user_count": &graphql.Field{
			Type: graphql.String,
			Resolve: func(p graphql.ResolveParams) (i interface{}, e error) {
				return 1000, nil
			},
		},
	}
	rootQuery := graphql.ObjectConfig{Name: "RootQuery", Fields: fields}
	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(rootQuery)}
	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		log.Fatalf("failed to create new schema, error: %+v", err)
	}
	*/

	schema, _ := graphql.NewSchema(
		graphql.SchemaConfig{
			Query:    gql.QueryType,
			Mutation: nil,
		})

	// [test] Query
	//query := `{ active_user_count }`
	//params := graphql.Params{Schema:schema, RequestString:query}
	//r := graphql.Do(params)
	//if len(r.Errors) > 0 {
	//	log.Fatalf("failed to execute graphql operation, errors: %+v", r.Errors)
	//}
	//rJSON, _ := json.Marshal(r)
	//fmt.Printf("%s \n", rJSON)

	// Test Server
	h := handler.New(&handler.Config{
		Schema:   &schema,
		Pretty:   true,
		GraphiQL: true,
	})

	http.Handle("/graphql", h)

	log.Println("graphql is running..")
	log.Fatal(http.ListenAndServe(":8999", nil))
}
